import random

def devinette():
    nombre_a_deviner = random.randint(1, 100)
    essais_restants = 10

    print("Devinez le nombre entre 1 et 100.")

    for essai in range(1, 11):
        guess = int(input("Essai #{} : ".format(essai)))
        
        if guess < nombre_a_deviner:
            print("Le nombre est plus grand.")
        elif guess > nombre_a_deviner:
            print("Le nombre est plus petit.")
        else:
            print("Félicitations ! Vous avez deviné le nombre en {} essais.".format(essai))
            break

        essais_restants -= 1
        print("Il vous reste {} essais.".format(essais_restants))

    if essais_restants == 0:
        print("Dommage ! Le nombre était {}.".format(nombre_a_deviner))

if __name__ == "__main__":
    devinette()     
